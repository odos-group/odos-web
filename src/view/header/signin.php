<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">


	<title>Odos</title>

	<link rel="shortcut icon" href="src/view/header/assets/images/gt_favicon.png">

	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="src/view/header/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="src/view/header/assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="src/view/header/assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="src/view/header/assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="index.html"><img src="src/view/header/assets/images/odos.png" width="70" height="70" alt="image"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li class="active"><a href="<?php echo $this->router->homePage(); ?>">Aceuil</a></li>
					<li><a href="<?php echo $this->router->getAboutPage(); ?>">A propos</a></li>

					<li><a href="<?php echo $this->router->getContactPage(); ?>">Contact</a></li>
					<li><a class="btn" href="<?php echo $this->router->getConnexionPage(); ?>">S'inscrire / S'identifier</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div>
	<!-- /.navbar -->

	<header id="head" class="secondary"></header>

	<!-- container -->
	<div class="container">

		<ol class="breadcrumb">
			<li><a href="src/view/header/headerAdmin.php">Accueil</a></li>
			<li class="active">Accés utilisateur</li>
		</ol>


		<div class="row">




			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
				<header class="page-header">
				</header>
				<h1 class="page-title">S'identifier</h1>

				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3 class="thin text-center">S'identifier à votre compte</h3>
							<p class="text-center text-muted">Vous pouvez<a href="<?php echo $this->router->getSignUpPage(); ?>">S'enregistrer</a> </p>
							<hr>

						<form action="<?php echo $this->router->getConnexion() ; ?>" method="POST" enctype="multipart/form-data">


								<?php $emailRef = $builder->getEmailRef(); ?>
								<div class="top-margin">
									<label>Email <span class="text-danger">*</span></label>
									<input type="text" name="<?php echo $emailRef; ?>" id="email" class="form-control">
								</div>
								<?php $passwordRef = $builder->getMotDePasseRef(); ?>
								<div class="top-margin">
									<label>Mot de passe <span class="text-danger">*</span></label>
									<input type="password" name="<?php echo $passwordRef; ?>" id="motpasse" class="form-control">
								</div>

								<hr>

								<div class="row">
									<div class="col-lg-8">
										<b><a href="">Mot de passe oublier ?</a></b>
									</div>

										<div class="col-lg-4 text-right">
											<button class="btn btn-action" type="submit">Valider</button>

										</div>


								</div>
							</form>
						</div>
					</div>

				</div>

			</article>
			<!-- /Article -->

		</div>
	</div>	<!-- /container -->


	<footer id="footer">

		<div class="footer1">
			<div class="container">
				<div class="row">

					<div class="col-md-3 widget">
						<h3 class="widget-title">Contacter nous</h3>
						<div class="widget-body">
							<p>+33 23 9873237<br>
								<a href="mailto:#">some.email@somewhere.com</a><br>
								<br>
								14000, CAEN
							</p>
						</div>
					</div>

					<div class="col-md-3 widget">
						<h3 class="widget-title">Suiver nous</h3>
						<div class="widget-body">
							<p class="follow-me-icons clearfix">
								<a href=""><i class="fa fa-twitter fa-2"></i></a>
								<a href=""><i class="fa fa-dribbble fa-2"></i></a>
								<a href=""><i class="fa fa-github fa-2"></i></a>
								<a href=""><i class="fa fa-facebook fa-2"></i></a>
							</p>
						</div>
					</div>



				</div> <!-- /row of widgets -->
			</div>
		</div>

		<div class="footer2">
			<div class="container">
				<div class="row">

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
								<a href="<?php echo $this->router->homePage(); ?>">Acueil</a> |
								<a href="<?php echo $this->router->getAboutPage(); ?>">A propos</a> |

								<a href="<?php echo $this->router->getContactPage(); ?>">Contact</a> |
								<b><a href="<?php echo $this->router->getConnexionPage(); ?>">S'enregistrer</a></b>
							</p>
						</div>
					</div>



				</div> <!-- /row of widgets -->
			</div>
		</div>
	</footer>





	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="src/view/header/assets/js/headroom.min.js"></script>
	<script src="src/view/header/assets/js/jQuery.headroom.min.js"></script>
	<script src="src/view/header/assets/js/template.js"></script>

	<!-- Google Maps -->
	<script src="https://maps.googleapis.com/maps/api/js?key=&amp;sensor=false&amp;extension=.js"></script>
	<script src="src/view/header/assets/js/google-map.js"></script>


</body>
</html>
