	<!DOCTYPE html>
			<html lang="fr">
				<head>
					<title><?php echo $this->title; ?></title>
					<meta charset="UTF-8"/>

				
				<!-- JS -->
					<script src="skin/js/bootstrap.min.js"></script>
					<script src="skin/js/jquery-3.2.1.min.js"></script>
					<script src="skin/js/bootstrap.js"></script>
					<script src="skin/js/jquery-ui.min.js"></script>
					<script src="skin/js/jquery.min.js"></script>
					<script src="skin/js/npm.js"></script>
					<script src="skin/js/jquery.min.js" type="text/javascript"></script>
					<script src="skin/js/jquery-ui.min.js" type="text/javascript"></script>
			
			<!-- CSS -->
					<link href="skin/css/jquery-ui/jquery-ui.min.css" type="text/css" rel="stylesheet">
					<link rel="stylesheet" href="skin/css/bootstrap.css">
					<link rel="stylesheet" href="skin/css/bootstrap.min.css">
					<link rel="stylesheet" href="skin/css/bootstrap-grid.css">
					<link rel="stylesheet" href="skin/css/bootstrap-grid.min.css">
					<link rel="stylesheet" href="skin/css/bootstrap-theme.min.css">
					<link rel="stylesheet" href="skin/style.css"/>
					
				</head>
				<body>

				<!-- Image and text -->
				<nav class="navbar navbar-light bg-light justify-content-between">
					<a class="navbar-brand" href="index.php">
						<img src="skin/img/logo.jpg" width="100" height="40" class="d-inline-block align-top" alt="">
					</a>
				</nav>

				<div class="container-fluid">
				<div class="row">

					<div class="container margeConexion">
						<main class="panel-connexion">

						<?php include_once 'view/notifications.php'; ?>
