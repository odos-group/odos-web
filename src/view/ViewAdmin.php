<?php
	require_once('Router.php');
	require_once('model/utilisateur/Utilisateur.php');
	require_once('model/utilisateur/UtilisateurBuilder.php');


	class ViewAdmin
	{

		protected $router;

		/** Titre de la page, rempli par une des méthodes make???Page() */
		protected $title;
		/** Contenu principal de la page, rempli par une des méthodes make???Page()  */
		protected $content;

		protected $header;

		public function __construct(Router $router)
		{
			$this->router = $router;
			$this->header = 'headerAdmin';
		}

		public function getClassName(){
			return "ViewAdmin";
		}


	################## Methodes utilisateurs ##########################

		public static function htmlesc($str)
		{
			return htmlspecialchars($str,
				/* on échappe guillemets _et_ apostrophes : */
				ENT_QUOTES
				/* les séquences UTF-8 invalides sont
				 * remplacées par le caractère �
				 * (au lieu de renvoyer la chaîne vide…) : */
				| ENT_SUBSTITUTE
				/* on utilise les entités HTML5 (en particulier &apos;) : */
				| ENT_HTML5,
				/* encodage */
				'UTF-8');
		}


		public function makeHomePage()
		{
			require_once 'header/'.$this->header.'.php';
		}

		public function makeConnexionPage(UtilisateurBuilder $builder)
		{
			#require_once 'header/'.$this->header.'signin'.'.php';
			require_once 'header/signin.php';
		}
		public function makeUtilisateurPage($nom, $score, $dis)
		{
			//var_dump($arr);
			#require_once 'header/'.$this->header.'signin'.'.php';
			require_once 'header/utilisateur.php';
		}

		public function makeAboutPage()
		{

			require_once 'header/about.php';
		}

		public function makeContactPage()
		{

			require_once 'header/contact.php';
		}

		public function makeSignUpPage()
		{

			require_once 'header/signup.php';
		}

		###############################"

		##################################"



	}
?>
