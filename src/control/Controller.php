<?php

	/* Inclusion des dépendances de cette classe */
	require_once('view/ViewAdmin.php');
	require_once('model/utilisateur/Utilisateur.php');
	require_once('model/utilisateur/UtilisateurStorage.php');

	/**
	 * Le contrôleur effectue les actions demandées
	 * et remplit la vue.
	 */
	class Controller
	{
		/** La vue utilisée par ce contrôleur pour afficher du HTML. */
		protected $view;

		/** La base de données contenant les patients. */
		protected $patientdb;
		protected $etatdb;
		protected $essai_cliniquedb;

		/**
		 * Construit un nouveau contrôleur.
		 */

		  public function __construct(ViewAdmin $view, Router $router)
		{
			$this->view            = $view;
			$this->router          = $router;
		}



		##################### Methodes Connexion ######################



#############################################
		public function detailUtilisateur($em)
		{

			var_dump($em);
			//$this->view->makeUtilisateurPage();
		}


		public function connect()
		{

			//var_dump($_POST);
			$email = $_POST['email'];
			$password = $_POST['mot_de_passe'];
			/*
				$url="https://dome7.ensicaen.fr:8443/api/authenticate";
				*/
				// Complétez le tableau associatif $postFields avec les variables qui seront envoyées par POST au serveur
				$postFields= array("email"=> $email, "password" => $password);
				$js = json_encode($postFields);
				/*
				//var_dump($js);
				// Tableau contenant les options de téléchargement
				$options=array(
				      CURLOPT_URL            => $url,       // Url cible (l'url de la page que vous voulez télécharger)
				      CURLOPT_RETURNTRANSFER => true,       // Retourner le contenu téléchargé dans une chaine (au lieu de l'afficher directement)
				      CURLOPT_HEADER         => false,      // Ne pas inclure l'entête de réponse du serveur dans la chaine retournée
				      CURLOPT_FAILONERROR    => true,       // Gestion des codes d'erreur HTTP supérieurs ou égaux à 400
				      CURLOPT_POST           => true,       // Effectuer une requête de type POST
				      CURLOPT_POSTFIELDS     => $js // Le tableau associatif contenant les variables envoyées par POST au serveur
				);
				*/
				$ch = curl_init('https://dome7.ensicaen.fr:8443/api/authenticate');
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $js);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				    'Content-Type: application/json',
				    'Content-Length: ' . strlen($js))
					);

					$result = curl_exec($ch);
					$tab = json_decode($result, true);
					//var_dump($tab);
					if ($tab["success"] == "true")
					{
						$t = array("email"=> $email);
						//var_dump($t);
						$js2 = json_encode($t);
						//var_dump($js2);
						$ch2 = curl_init('https://dome7.ensicaen.fr:8443/api/user');
						curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");
						curl_setopt($ch2, CURLOPT_POSTFIELDS, $js2);
						curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
						    'Content-Type: application/json',
						    'Content-Length: ' . strlen($js2))
							);

							$result2 = curl_exec($ch2);
							$tab2 = json_decode($result2, true);
							//var_dump($tab2);
							//$par = array('nickname' => $tab2['nickname'], 'score' => $tab2['score'], 'distance' => $tab2['distance']);
							$par = array('nickname' => $tab2['nickname'], 'score' => 300, 'distance' => $tab2['distance']);
							//var_dump($par);
							//var_dump($par);
						//$postFields["erreur"] = "cette utilisateur pas";
						//$this->view->makeDeniedAccessPage();
						//var_dump($tab);
						//$this->view->makeUtilisateurPage($par);
						//$this->view->makeUtilisateurPage($par);
						$this->router->POSTredirect($this->router->makePage($tab2['nickname'],300, $tab2['distance']));
					}
					else
					{

						$this->router->POSTredirect($this->router->getConnexionPage());

						//$this->router->getConnexionPage();
						//$postFields["erreur"] = "cette utilisateur pas";
						//$this->view->makeUtilisateurPage($tab);
					}
					//var_dump($result);
					/*
				////////// MAIN

				// Création d'un nouvelle ressource cURL
				$CURL=curl_init();

				// Erreur suffisante pour justifier un die()
				if(empty($CURL)){die("ERREUR curl_init : Il semble que cURL ne soit pas disponible.");}

				      // Configuration des options de téléchargement
				      curl_setopt_array($CURL,$options);

				      // Exécution de la requête
				      $content=curl_exec($CURL);            // Le contenu téléchargé est enregistré dans la variable $content. Libre à vous de l'afficher.
							var_dump($content);
				      // Si il s'est produit une erreur lors du téléchargement
				      if(curl_errno($CURL)){
				            // Le message d'erreur correspondant est affiché
				            echo "ERREUR curl_exec : ".curl_error($CURL);
				      }

				// Fermeture de la session cURL
				*/
				curl_close($ch);
				//$this->view->makeUtilisateurPage($postFields);


			/*
			$url="https://dome7.ensicaen.fr:8443/api/users";

			// Tableau contenant les options de téléchargement
			$options=array(
			      CURLOPT_URL            => $url, // Url cible (l'url la page que vous voulez télécharger)
			      CURLOPT_RETURNTRANSFER => true, // Retourner le contenu téléchargé dans une chaine (au lieu de l'afficher directement)
			      CURLOPT_HEADER         => false // Ne pas inclure l'entête de réponse du serveur dans la chaine retournée
			);

			////////// MAIN

			// Création d'un nouvelle ressource cURL
			$CURL=curl_init();

			      // Configuration des options de téléchargement
			      curl_setopt_array($CURL,$options);

			      // Exécution de la requête
			      $content=curl_exec($CURL);      // Le contenu téléchargé est enregistré dans la variable $content. Libre à vous de l'afficher.
						var_dump($content);
			// Fermeture de la session cURL
			curl_close($CURL);
			*/
		}









		###################### Methodes utilisateurs ######################

		/**
		 * Fait le nécessaire pour que la vue puisse
		 * afficher la liste de tous les utilisateurs.
		 */
}
