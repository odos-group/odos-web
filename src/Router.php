<?php
	/* Inclusion des dépendances de cette classe */
	require_once('view/ViewAdmin.php');
	require_once('control/Controller.php');
	require_once('model/utilisateur/UtilisateurBuilder.php');
	require_once('model/utilisateur/UtilisateurStorage.php');


	session_start();

	/**
	 * Le routeur s'occupe d'analyser les requêtes HTTP
	 * pour décider quoi faire et quoi afficher.
	 * Il se contente de passer la main au contrôleur et
	 * à la vue une fois qu'il a déterminé l'action à effectuer.
	 *
	 * Le routeur est censé être le seul à connaître les URL du site:
	 * toute classe ayant besoin d'une URL la lui demandera.
	 */
	class Router
	{
		/**
		 * Point d'entrée de l'application.
		 * Gère la requête courante en transférant le contrôle
		 * au contrôleur ou à la vue appropriés.
		 */

		public function main()
		{
			$view = new ViewAdmin($this);

			/* Récupération de l'utilisateur dans l'URL */
			$nomUT = isset($_GET['id']) ? $_GET['id'] : null;
			$scoreUT = isset($_GET['id2']) ? $_GET['id2'] : null;
			$disUT = isset($_GET['id3']) ? $_GET['id3'] : null;
			$email = isset($_GET['email']) ? $_GET['email'] : null;
			$action = isset($_GET['action']) ? $_GET['action'] : null;
			$tab = isset($_GET) ? $_GET : null;

			//$utId = isset($_GET['id']) ? $_GET['id'] : null;


			/* si aucune utilisateur n'est connecté
			 * et que 'lon n'est pas dans la procédure de connexion
			 * alors on reste sur la page de log*/




			$ctl = new Controller($view, $this);

			/* On remplit la vue en fonction de l'URL.*/

						if ($action !== null) {
							switch ($action) {


								case "connexion" :
									$view->makeConnexionPage(new UtilisateurBuilder(array()));
									break;
								case "about" :
									$view->makeAboutPage();
									break;
								case "contact" :
									$view->makeContactPage();
									break;
								case "signUp" :
									$view->makeSignUpPage();
									break;
								case "page" :
									//var_dump($scoreUT);
									//var_dump($utId);
									$view->makeUtilisateurPage($nomUT, $scoreUT, $disUT);
									break;


								case "connect" :
									//var_dump($_POST);
									$ctl->connect($_POST);
									break;
								case "detailUtilisateur" :
									var_dump($email);
									//$ctl->detailUtilisateur();
									break;


							}

						} else {
								/* pas d'id donné dans l'URL: on affiche la page d'accueil */
								$view->makeHomePage();
							}
						echo "<script>console.log('".$view->getClassName()."');</script>";
					}


		/** URL de la page d'accueil */
		public function homePage()
		{
			return "appli.php";
		}




		############## Methodes utilisateurs ###################


		/**
		 * URL de la page A propos
		 */
		 public function aboutPage()
		 {
			 return ".?action=aPropos";
		 }



		 /**
		  * URL de la page de connexion
		  */
		 public function getConnexionPage()
		 {
			 return "appli.php?action=connexion";
		 }

		 public function getAboutPage()
		{
			return "appli.php?action=about";
		}
		public function getContactPage()
		{
			return "appli.php?action=contact";
		}

		public function getSignUpPage()
		{
			return "appli.php?action=signUp";
		}

		public function makePage($nom, $scor, $dis)
		{
			//var_dump($nom);
			//var_dump($arr);
			//$nom = $arr['nickname'];
			//$score = $arr['score'];
			//$distance = $arr['distance'];
			//$id = 2;

			return "appli.php?id=$nom&amp;id2=$scor&amp;id3=$dis&amp;action=page";
		}





		 public function getConnexion()
		 {
			 return "appli.php?action=connect";
		 }

		public function detailUtilisateur()
 		{

 			return "appli.php?action=detailUtilisateur";
 		}
		/**
		 * Redirige l'internaute vers l'url donnée en argument, via
		 * une redirection HTTP 303. Destinée à être utilisé
		 * après un POST, sur les URL données par le routeur
		 * (dont il faut donc décoder les entités HTML)
		 */
		public function POSTredirect($url)
		{
			header("Location: " . htmlspecialchars_decode($url), true, 303);
			die;
		}

	}
