<?php
	/**
	 * Vérifie si l'élément passé en paramètre est contenu dans le tableau.
	 *
	 * @param $array
	 * @param $element
	 * @param $colonne
	 *
	 * @return mixed
	 */
	function verifierElementExiste($array, $element, $colonne)
	{
		foreach ($array as $key => $value) {
			if ($element == $value->$colonne()) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Permet de convertir une date anglaise au format français.
	 *
	 * @param $date
	 *
	 * @return bool|string
	 */
	function dateToFr($date)
	{
		return date("d-m-Y", strtotime($date));
	}

		/**
	 * Permet de convertir une date anglaise au format français en string.
	 *
	 * @param $date
	 *
	 * @return bool|string
	 */
	 function dateToFrStr($date)
	 {
		 return substr($date, 8, 2).'-'.substr($date, 5, 2).'-'.substr($date, 0, 4);
	 }


	/**
	 * Permet de convertir une date francaise au format anglais.
	 *
	 * @param $date
	 *
	 * @return bool|string
	 */
	 function dateToEng($date)
	 {
		 return date("Y-m-d", strtotime($date));
	 }

	 /**
	 * Permet de recuperer une date sans le jour.
	 *
	 * @param $date
	 *
	 * @return bool|string
	 */
	 function shrinkDate($date)
	 {
		list($year, $month, $day) = preg_split("/[\/\.-]+/", $date);
		return $month."-".$year;
	 }

	 
	 function no_accent($str)
	 {
		if (preg_match("#[a-zA-Z0-9]#", $str)){
          return true;
    	} else {
          return false;
    	}
	 }

 	/**
	 * Permet de recuperer les trois lettre du nom.
	 *
	 * @param $nom
	 *
	 * @return bool|string
	 */
	 function nomThree($nom)
	 {
		mb_internal_encoding('UTF-8');
		$third = mb_substr($nom, 0, 3);

		return $third;
	 }

	 /**
	 * Permet de recuperer les deux lettre du prenom.
	 *
	 * @param $prenom
	 *
	 * @return bool|string
	 */
	 function prenomTwo($prenom)
	 {
		
		mb_internal_encoding('UTF-8');
		$second = mb_substr($prenom, 0, 2);

		return $second;
	 }

	function strtolowerFr($string) {

		$firstCarac = substr($string, 0, 1);

		if (preg_match("#[a-z0-9]#", $firstCarac)){
			$firstCarac = ucfirst($firstCarac);
			$str = mb_strtolower(substr($string, 1));
			return $firstCarac . $str;
		} else {
			$firstCarac = substr($string, 0, 2);
			$firstCarac = str_replace(
				array('é', 'è', 'ê', 'ë', 'à', 'â', 'î', 'ï', 'ô', 'ù', 'û'),
				array('É', 'È', 'Ê', 'Ë', 'À', 'Â', 'Î', 'Ï', 'Ô', 'Ù', 'Û'),
				$firstCarac
			);
			$str = mb_strtolower(substr($string, 2));
			return $firstCarac . $str;
		}
	}

	/**
	 * Permet de convertir une date anglaise au format français en string.
	 *
	 * @param $date
	 *
	 * @return bool|string
	 */
	function dateHistorique($date)
	{
		return substr($date, 8, 2).'/'.substr($date, 5, 2).'/'.substr($date, 0, 4) . ' ' . substr($date, 10);
	}