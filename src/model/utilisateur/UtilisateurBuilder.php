<?php

	/* Inclusion des dépendances de cette classe */
	require_once("model/utilisateur/Utilisateur.php");

	/**
	 * Représente une instance de Utilisateur en cours de manipulation
	 * ( création ou modification )
	 */
	class UtilisateurBuilder
	{
		/** Les données permettant de construire l'instance d'un utilisateur. */
		protected $data;

		/** Les erreurs sur les données. */
		private $error;

		/**
		 * Construit une nouvelle instance à partir d'un tableau de données.
		 */
		public function __construct(array $data)
		{
			/** Verification de l'existence des champs */
			if (!isset($data['id']))
				$data['id'] = null;
			if (!isset($data['pseudo']))
				$data['pseudo'] = '';
			if (!isset($data['mot_de_passe']))
				$data['mot_de_passe'] = '';
			if (!isset($data['email']))
				$data['email'] = '';
			if (!isset($data['score']))
				$data['score'] = null;
			$this->data = $data;
			$this->erro = null;
		}


		/**
		 * Renvoie le tableau de données
		 */
		public function getData()
		{
			return $this->data;
		}

		/**
		 * Renvoie les erreurs sur les données courantes,
		 * ou null si les données n'ont pas été validées.
		 */
		public function getError()
		{
			return $this->error;
		}

		/**
		 * Construit une instance de Utilisateur avec les données de cette instance
		 */
		public function createUtilisateur()
		{
			//$hash_PS = hash("sha256",$this->data['pseudo']));
			//$hash_PW = hash("sha256",$this->data['mot_de_passe']));
			$hash_PS = hash("sha256",$this->data['pseudo']);
			$hash_PW = hash("sha256",$this->data['mot_de_passe']);
			return new Utilisateur($hash_PS, $hash_PW, $this->data['email'],$this->data['score']);
		}

		/**
		 * Met à jour une instance d'un utilisateur avec les données fournies.
		 */
		public function updateUtilisateur(Utilisateur $utilisateur)
		{
			if (isset($this->data['pseudo'])){
				#$this->data['pseudo'] = $this->data['pseudo'];
				if (!self::isPseudoValid($this->data['pseudo']))
					throw new Exception("Le pseudo est invalide. ");
				$utilisateur->setPseudo($this->data['pseudo']);
			}
			if (isset($this->data['mot_de_passe'])){
				#$this->data['pseudo'] = $this->data['pseudo'];
				if (!self::isMotDePasseValid($this->data['mot_de_passe']))
					throw new Exception("Le mot de passe est invalide. ");
				$utilisateur->setPseudo($this->data['mot_de_passe']);
			}

			if (isset($this->data['email'])){
				if (!self::isEmailValid($this->data['email']))
					throw new Exception("L'email est invalide. ");
				$utilisateur->setEmail($this->data['email']);
			}
			if (isset($this->data['score']))
				$utilisateur->setScore($this->data['score']);

			return $utilisateur;
		}

		/**
		 * Renvoie une nouvelle instance de UtilisateurBuilder avec les données
		 * modifiables du utilisateur passée en argument.
		 */
		public static function buildFromUtilisateur(Utilisateur $utilisateur)
		{
			return new UtilisateurBuilder(array(
				"id"			=> $utilisateur->getId(),
				"pseudo"		=> $utilisateur->getPseudo(),
				"mot_de_passe"	=> $utilisateur->getMotDePasse(),
				"email"	=> $utilisateur->getEmail(),
				"score"	=> $utilisateur->getScore()
			));
		}


		/**
		 * Vérifie que les données sont valides.
		 */
		public function isValid()
		{
			$this->error = '';
			if ($this->isPseudoValid($this->data['pseudo']) == false)
				$this->error .= 'Le pseudo de l\'utilisateur est invalide. ';
			if ($this->isMotDePasseValid($this->data['mot_de_passe']) == false)
				$this->error .= 'Le mot de passe de l\'utilisateur est invalide. ';
			if ($this->isEmailValid($this->data['email']) == false)
				$this->error .= 'L\'email de l\'utilisateur est invalide.' ;
			if ($this->isScoreValid($this->data['score']) == false)
				$this->error .= 'Le score est invalide. ';
			return $this->error === '';
		}

		/**
		 * Renvoie la clef utilisé pour le pseudo de l'utilisateur dans le tableau de données.
		 */
		public function getPseudoRef()
		{
			return 'pseudo';
		}

		/**
		 * Renvoie la clef utilisé pour le mot de passe de l'utilisateur dans le tableau de données.
		 */
		public function getMotDePasseRef()
		{
			return 'mot_de_passe';
		}

		/**
		 * Renvoie la clef utilisé pour l'email de l'utilisateur dans le tableau de données.
		 */
		public function getEmailRef()
		{
			return 'email';
		}

		/**
		 * Renvoie la clef utilisé pour le score de l'utilisateur dans le tableau de données.
		 */
		public function getScoreRef()
		{
			return 'score';
		}

		/**
		 * Indique si $pseudo est un pseudo valide pour un utilisateur.
		 * Il doit fair moins de 30 caractères et ne pas être vide
		 */
		public static function isPseudoValid($pseudo)
		{
			return mb_strlen($pseudo, 'UTF-8') < 30 && $pseudo !== "";
		}


		/**
		 * Indique si $mot_de_passe est un mot de passe valide pour un utilisateur.
		 * Il doit fair moins de 30 caractères et ne pas être vide
		 */
		public static function isMotDePasseValid($mot_de_passe)
		{
			return $mot_de_passe !== "";
		}
		/**
		 * Indique si $email est un email valide pour un utilisateur.
		 * Il doit ne pas être vide
		 */
		public static function isEmailValid($email)
		{
			return $email !== "";
		}
		/**
		 * Indique si Score bien un chiffre
		 */
		public static function isScoreValid($score)
		{
			return $score <= 100000 && $niveau >= 0 ;
		}


	}
