<?php

	/* Inclusion des dépendances de cette classe */
	require_once("model/utilisateur/Utilisateur.php");

	/**
	 * Interface pour isoler du reste de l'application
	 * les questions relatives ai stockage des utilisateurs
	 */
	interface UtilisateurStorage
	{
		/**
		 * Renvoie l'instance de utilisateur ayant pour identifiant
		 * celui passé en argument, ou null s'il n'en existe pas
		 */
		public function read($id);


		/**
		 * Renvoie l'utilisateur lié au professionnel
		 */
		 public function readUtilisateur($id);

		/**
		 * Renvoie l'instance de utilisateur ayant pour pseudo
		 * celui passé en arguement, ou null s'il n'en existe pas
		 */
		public function readByPseudo($pseudo);

		/**
		 * Renvoie l'instance de utilisateur ayant pour pseudo et mot de passe
		 * ceux passés en arguement, ou null s'il n'en existe pas
		 */
		public function readByInfos($pseudo, $mot_de_passe);

		/**
		 * Renvoie un tableau contenant tous les utilisateurs de la base,
		 * indexés par leur identifiant.
		 */
		public function readAll();

		/**
		 * Ajoute un utilisateur dans la base de donnee
		 */
		public function create(Utilisateur $utilisateur);

		/**
		 * Modifie un utilisateur de la bdd
		 */
		public function update($id, Utilisateur $utilisateur);


		/**
		 * Supprime un utilisateur de la base.
		 */
		 public function delete($id);


		 /**
		 * verification si l'utilisateur est unique.
		 */
		 public function verifExistePseudo($pseudo);

	}
