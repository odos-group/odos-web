<?php

	/* Inclusion des dépendances de cette classe */
	require_once("model/utilisateur/Utilisateur.php");
	require_once("model/utilisateur/UtilisateurStorage.php");

	/**
	 * Permet de gérer des utilisateurs stockés dans une
	 * base de données MySql
	 */
	class UtilisateurStorageMySql implements UtilisateurStorage
	{
		/** L'instance de PDO utilisé pour communiquer avec le serveur MySql. */
		protected $pdo;

		/**
		 * Construit une nouvelle instance, qui utilise l'instance
		 * de PDO donnée en argument.
		 */
		public function __construct($pdo)
		{
			$this->pdo = $pdo;
		}

		/** Implémentation de la méthode de UtilisateurStorage */
		public function read($id)
		{
			$query = 'SELECT * FROM `utilisateur` WHERE `id_utilisateur` = :id;';
			$st = $this->pdo->prepare($query);
			$st->execute(array(':id' => $id));
			$arr = $st->fetch();
			/** si le tableau est vide, c'est que la requête n'a rien renvoyé */
			if(!$arr)
				return null;
			/** on construit le utilisateur à partir des informations récupérées */
			return new Utilisateur($arr['id_utilisateur'], $arr['pseudo'], $arr['mot_de_passe'], $arr['email'], $arr['score']);
		}

		/** Implémentation de la méthode de UtilisateurStorage */
		public function readUtilisateur($id)
		{
			/** On verifie que l'utilisateur existe */
			$query = 'SELECT * FROM `utilisateur` WHERE `id_professionnel` = :id;';
			$st = $this->pdo->prepare($query);
			$st->execute(array(':id' => $id));
			$arr = $st->fetch();
			/** si le tableau est vide, c'est que la requête n'a rien renvoyé */
			if(!$arr["id_utilisateur"]){
				return null;
			} else {
				return new Utilisateur($arr['id_utilisateur'], $arr['pseudo'], $arr['mot_de_passe'], $arr['email'], $arr['score']);
			}
		}


		/** Implémentation de la méthode de UtilisateurStorage */
		public function readByPseudo($pseudo)
		{
			$query = 'SELECT * FROM `utilisateur` WHERE `pseudo` = :pseudo;';
			$st = $this->pdo->prepare($query);
			$st->execute(array(':pseudo' => $pseudo));
			$arr = $st->fetch();
			/** si le tableau est vide, c'est que la requête n'a rien renvoyé */
			if(!$arr)
				return null;
			/** on construit le utilisateur à partir des informations récupérées */
			return new Utilisateur($arr['id_utilisateur'], $arr['pseudo'], $arr['mot_de_passe'], $arr['email'], $arr['score']);
		}

		/** Implémentation de la méthode de UtilisateurStorage */
		public function readByInfos($pseudo, $mot_de_passe)
		{
			$query = 'SELECT * FROM `utilisateur` WHERE `pseudo` = :pseudo and `mot_de_passe` = :mot_de_passe';
			$st = $this->pdo->prepare($query);
			$st->execute(array(':pseudo' => $pseudo, ':mot_de_passe' => $mot_de_passe));
			$arr = $st->fetch();
			/** si le tableau est vide, c'est que la requête n'a rien renvoyé */
			if(!$arr)
				return null;
			/** on construit le utilisateur à partir des informations récupérées */
			return new Utilisateur($arr['id_utilisateur'], $arr['pseudo'], $arr['mot_de_passe'], $arr['email'], $arr['score']);
		}

		/** Implementation de la méthode de UtilisateurStorage */
		public function readAll()
		{
			$query = 'SELECT * FROM `utilisateur`;';
			$st = $this->pdo->prepare($query);
			$st->execute();

			/* On crée le tableau associatif */
			$utilisateurs = array();
			while ($arr = $st->fetch()) {
				$utilisateurs[$arr['id_utilisateur']] = new Utilisateur($arr['id_utilisateur'], $arr['pseudo'], $arr['mot_de_passe'], $arr['email'], $arr['score']);
			}

			return $utilisateurs;
		}

		/** Implementation de la méthode de UtilisateurStorage */
		public function create(Utilisateur $utilisateur)
		{

			$query = 'INSERT INTO `utilisateur` (`pseudo`, `mot_de_passe`, `email`, `score`) VALUES (:pseudo, :mot_de_passe, :eamil, :score);';
			$st = $this->pdo->prepare($query);
			$res = $st->execute(array(
				':pseudo' => $utilisateur->getPseudo(),
				':mot_de_passe' => $utilisateur->getMotDePasse(),
				':email' => $utilisateur->getEmail(),
				':score' => $utilisateur->getScore(),
			));
			$id_utilisateur = $this->pdo->lastInsertId();

			if($res) return $id_utilisateur;
			else return null;
		}

		/** Implémentation de la méthode de UtilisateurStorage */
		public function update($id_utilisateur, Utilisateur $utilisateur)
		{

			$query = 'UPDATE `utilisateur` SET `pseudo` = :pseudo, `mot_de_passe` = :mot_de_passe, `email`=:email, `score` = :score WHERE `id_utilisateur` = :id;';

			$st    = $this->pdo->prepare($query);
			$res = $st->execute(array(
				':id' => $id_utilisateur,
				':pseudo' => $utilisateur->getPseudo(),
				':mot_de_passe' => $utilisateur->getMotDePasse(),
				':email' => $utilisateur->getEmail(),
				':score' => $utilisateur->getScore(),
			));

			if($res){
				return $id_utilisateur;
			}
			else{
				return null;
			}
		}



		/** Implémentation de la méthode de UtilisateurStorage */
		public function delete($id)
		{
			$query = 'DELETE FROM `utilisateur` WHERE `id_utilisateur` = :id';
			$st    = $this->pdo->prepare($query);
			$st->execute(array(':id' => $id));
			return $st->rowCount() !== 0;
		}

		/** Implémentation de la méthode de verification si l'utilisateur est unique */
		public function verifExistePseudo($pseudo)
		{
			$query = 'SELECT COUNT(pseudo) as countPseudo FROM `utilisateur` WHERE `pseudo` = :pseudo';
			$st = $this->pdo->prepare($query);
			$st->execute(array(':pseudo' => $pseudo,));
			$arr = $st->fetch();

			/** si le tableau est vide, c'est que la requête n'a rien renvoyé */
			if(!$arr)
				return 0;
			/** on construit le nombre d'utilisateur */
			return $arr;
		}


	}
