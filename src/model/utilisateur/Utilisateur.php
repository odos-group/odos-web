<?php

	/**
	 *
	 */
	class Utilisateur
	{
		/** L'id de l'utilisateur */
		protected $id;

		/** Le pseudo de l'utilisateur */
		protected $pseudo;

		/** Le mot de passe de l'utilisateur */
		protected $mot_de_passe;

		/** L'email de l'utilisateur */
		protected $email;

		/** Le Score de l'utilisateur */
		protected $score;

		/**
		 * Construit un nouveau utilisateur
		 */
		public function __construct($id, $pseudo, $mot_de_passe, $email, $score)
		{
			$this->id = $id;
			$this->pseudo = $pseudo;
			$this->mot_de_passe = $mot_de_passe;
			$this->email = $email;
			$this->score = $score;
		}

		/**
		 * Renvoie l'id de l'utilisateur.
		 */
		public function getId()
		{
			return $this->id;
		}

		/**
		 * Renvoie le pseudo de l'utilisateur.
		 */
		public function getPseudo()
		{
			return $this->pseudo;
		}

		/**
		 * Renvoie le mot_de_passe de l'utilisateur.
		 */
		public function getMotDePasse()
		{
			return $this->mot_de_passe;
		}

		/**
		 * Renvoie l'email de l'utilisateur
		 */
		public function getEmail()
		{
			return $this->email;
		}

		/**
		 * Renvoie le score de l'utilisateur
		 */
		public function getScore()
		{
			return $this->score;
		}


		/**
		 * Modifie le pseudo de l'utilisateur
		 */
		public function setPseudo($pseudo)
		{
			$this->pseudo = $pseudo;
		}

		/**
		 * Modifie le mot de passe de l'utilisateur
		 */
		public function setMotDePasse($mot_de_passe)
		{
			$this->mot_de_passe = $mot_de_passe;
		}

		/**
		 * Modifie l'email de l'utilisateur
		 */
		public function setEmail($email)
		{
			$this->email = $email;

		}

		/**
		 * Modifie le score de l'utilisateur
		 */
		public function setScore($premiere_connexion)
		{
			$this->score = $score;
		}

		/**
		 * Verifie si le mot de passe entree en arguement
		 * est egal au mot de passe de l'utilisateur
		 */
		public function verifMotDePass($mot_de_passe)
		{
			return ($this->mot_de_passe == $mot_de_passe);
		}

	}
