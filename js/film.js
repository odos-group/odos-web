var li     = document.getElementById('ul').getElementsByTagName('li');
var newDiv = document.createElement("div");

var filmPage = document.createElement("div");

newDiv.classList.add("infobulle");


for (var i = 0; i < li.length; i++) {

    li[i].addEventListener('mouseenter', function () {

        href = this.querySelector('a').getAttribute("href");

        /** */
        var xhr = new XMLHttpRequest();
        xhr.open('GET', href);
        xhr.addEventListener("load", function () {
            filmPage.innerHTML = xhr.response; //Si ce qu'on a fait avait fonctionné, il aurait fallu faire une nouvelle vue d'un film plus ordonné, juste prévu pour le JS
        }, false);
        xhr.send();

        newDiv.style.display = 'block';
        newDiv.textContent   = this.textContent;

        //Rajoute les éléments à la "bulle"
        var info      = filmPage.getElementsByTagName("p");
        var info_film = "";
        for (var j = 0; j < info.length; j++) {
            //Permet de renvoyer les infos du films dans la vue avec le titre du film, nous avons mis cela en commentaire puisque ça fonctionne mal.
            //newDiv.textContent += info[j].textContent;
            //Affiche les infos du films dans la console mais on affiche les infos du films qu'on a était voir avant puisque il affiche 2 fois les infos du film avant de passer à l'autre
            info_film += info[j].textContent;
        }
        //Affiche le problème en console
        console.log(info_film);

        this.appendChild(newDiv);
    });

    li[i].addEventListener('mouseleave', function () {

        newDiv.style.display = 'none';
        this.removeChild(newDiv);

    });
}