<?php
	/**
	 * On indique que les chemins des fichiers qu'on inclut
	 * seront relatifs au répertoire src.
	 */
	set_include_path("./src");

	/* Inclusion des classes utilisées dans ce fichier */
	require_once("Router.php");
	require_once("model/utilisateur/UtilisateurStorageMySql.php");


	/**
	 * On récupère les identifiants de connexion qui sont stockés
	 * en-dehors de l'espace accessible par le web.
	 */


	/**
	 *On crée une instance de PDO.
	 */


	//$utilisateurdb = new UtilisateurStorageMySQL($pdo);

	/**
	 * Cette page est simplement le point d'arrivée de l'internaute
	 * sur notre site. On se contente de créer un routeur
	 * et de lancer son main.
	 */

	$router = new Router();
	$router->main();
